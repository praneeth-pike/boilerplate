//Include Gulp
var gulp = require('gulp');

//Include Plugins
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
// var imagemin = require('gulp-imagemin');
var image = require('gulp-image');
var htmlmin = require('gulp-htmlmin');
var cleanCSS = require('gulp-clean-css');
var cache = require('gulp-cache');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var include = require('gulp-include');
var fileinclude = require('gulp-file-include'),
    autoprefixer = require('gulp-autoprefixer'),
    connect = require('gulp-connect-php');
    

//Path Variables
var source = "src/";
var dest = "build";

//static server + watching scss/html files
gulp.task('serve', ['sass','filesInclude', 'scripts'], function () {
    'use strict'; 
    browserSync({
        server: {
            baseDir: "build"
        },
        files: "./src/scss/*.scss"
    });
    gulp.watch("src/scss/*.scss", ['sass']).on('change', reload);
    gulp.watch("src/**/*.js", ['scripts']).on('change', reload);
    gulp.watch("src/**/*.png", ['images']);
    // gulp.watch("src/***/**/*.svg", ["images"]);
    gulp.watch("src/**/*.html", ['filesInclude']).on('change', reload);
    
});


//php server
gulp.task('connect', function() {
    connect.server({
        base: './build',
        port: '3200'
    });
});

//Concatenate & Minify JS files
gulp.task('scripts', function () {
    'use strict';
    return gulp
        .src('./src/js/core.js')
        .pipe(include())
        .pipe(concat('app.js')) 
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('./build/js'));
});
//Css Compiler
gulp.task('sass', function () {
    'use strict';
    return gulp
        .src('src/scss/app.scss')
        .pipe(sass())
        .pipe(autoprefixer({
            browsers:['last 4 versions'],
            cascade:false
        }))
        .pipe(cleanCSS())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('build/css'))
        .pipe(reload({stream: true}));
});
//Image Optimization
gulp.task('images', function () {
    'use strict';
    return gulp
        .src('src/img/**/*')
        // .pipe(cache(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true })))
        .pipe(image({
            pngquant: true,
          optipng: false,
          zopflipng: true,
          advpng: true,
          jpegRecompress: false,
          jpegoptim: true,
          mozjpeg: true,
          gifsicle: true,
          svgo: true
        }))
        .pipe(gulp.dest('build/img'));
});

gulp.task('filesInclude', function () {
    gulp.src(['src/*.html', 'src/**/*.html'])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: 'src/'
        }))
        .pipe(gulp.dest("build/"));
});

//Watch files for Changes
gulp.task('watch', function () {
    'use strict';
    gulp.watch('src/**/*.html', ['filesInclude']);
    gulp.watch('src/scss/*.scss', ['sass']);
    gulp.watch('src/js/*.js', ['scripts']);
    gulp.watch('src/img/*', ['images']);  
});

//Default Task
gulp.task('default', ['scripts', 'sass', 'watch','filesInclude','images', 'serve']);
